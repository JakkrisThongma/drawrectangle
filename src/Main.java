import java.util.Scanner;


public class Main{

    public static int getWidth()
    {
        Scanner sc = new Scanner(System.in);
        int i = 0;
        while(i<2)
        {
            try{
                sc = new Scanner(System.in);
                i = sc.nextInt();
                if(i<1)
                {
                    System.out.println("Number needs to be greater than 2, please try again");
                }
            }
            catch (java.util.InputMismatchException e)
            {
                System.out.println("Invalid input, please try again");
            }
            
        }
        return i;
    }

    public static int getHeight(int firstNumber)
    {
        Scanner sc = new Scanner(System.in);
        int i = 0;
        while(i<2 || i == firstNumber)
        {
            try{
                sc = new Scanner(System.in);
                i = sc.nextInt();
                if(i<2)
                {
                    System.out.println("Number needs to be greater than 2, please try again");
                }
                else if(i == firstNumber)
                {
                    System.out.println("1st number and 2nd number can not be equal to each other, please try again");
                }
            }
            catch (java.util.InputMismatchException e)
            {
                System.out.println("Invalid input, please try again");
            }
            
        }
        return i;
    }

    public static void drawRectangle(int width, int height)
    {
    //width = 5
    //height = 10
       for (int i = 1; i <= height; i++) {
        for (int j = 1; j <= width; j++) {
            if (i == 1 || i == height || j == 1 || j == width)
                System.out.print("#");
            else
                System.out.print(" ");
        }
        System.out.print("\n");
        }
    }


    public static void main(String args[])
    {
        boolean run = true;
        while(run)
        {
            System.out.println("Please enter width");
            int width = getWidth();

            System.out.println("Please enter height");
            int height = getHeight(width);
            drawRectangle(width, height);
            
            System.out.println("Type q and enter to exit, or enter any keys to continue");
            Scanner sc = new Scanner(System.in);
            char c = sc.next().charAt(0); 
            if(c == 'q')
            {
                run = false;
            }
           
        }
        
    }

}

